#!/bin/bash -e

apt-get update
apt-get install -y python3 python3-pip curl git

curl -sSL https://install.python-poetry.org | python3 -

echo 'export PATH="/root/.local/bin:$PATH"' >> ~/.bashrc

curl https://pyenv.run | bash

echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
