packer {
  required_plugins {
    docker = {
      version = ">= 0.0.7"
      source = "github.com/hashicorp/docker"
    }
  }
}

variable "login_username" {  
  type = string
  default = "username"
}

variable "login_password" {  
  type = string
  default = "password"
}

source "docker" "ubuntu" {
  image  = "ubuntu"
  commit = true
}

build {
  sources = [
    "source.docker.ubuntu"
  ]

  provisioner "shell" {
    script = "./scripts/install.sh"
  }

  post-processors {
    post-processor "docker-tag" {
      repository = "mattdark/ubuntu-docker"
      tags       = ["latest"]
      only       = ["docker.ubuntu"]
    }

    post-processor "docker-push" {
      login          = true
      login_username = var.login_username
      login_password = var.login_password
    }
  }
}
